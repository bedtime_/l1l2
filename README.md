# l1l2

This program takes an .srt, .txt, .pdf, or .epub file, translates it, and merges both translations into a .txt (or.ssa) file in an interlinear fashion, allowing both texts to be viewed side by side.
